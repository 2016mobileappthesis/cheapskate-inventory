#include "Order.h"

using namespace System;
using namespace System::Windows::Forms;
using namespace std;

Order::orderStatus Order::order::_orderStatus;
float Order::order::_orderPrice;
std::vector<Inventory::Item> Order::order::orderList;

void Order::order::setOrderStatus(Order::orderStatus status) {
	_orderStatus = status;
}

Order::orderStatus Order::order::getOrderStatus(void) {
	return _orderStatus;
}

void Order::order::setOrderPrice(float orderPrice) {
	_orderPrice = orderPrice;
}

float Order::order::getOrderPrice(void) {
	return _orderPrice;
}
