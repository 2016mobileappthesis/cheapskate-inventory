#ifndef __INVENTORY_H_INCLUDED__
#define __INVENTORY_H_INCLUDED__

#pragma once
#include <vector>

using namespace System;
using namespace std;

namespace Inventory {
	
	//item class
	class Item
	{
	public:
		void setName(string name) { _itemName = name; };
		string getName(void) { return _itemName; };
		void setPrice(float price) { _price = price; };
		float getPrice(void) { return _price; };
		void setIndex(int index) { _index = index; };
		int getIndex(void) { return _index; };
		string getCategory(void) { return _category; };
		void setAll(string name, string category, float price, int index) {
			_itemName = name;
			_category = category;
			_price = price;
			_index = index;
		}
		Item() {};
		~Item() {};
		
	private:
		string _itemName;
		int _index;
		float _price;
		string _category;
	};

	class Category {		//all possible categories
	public:
		static const string ENTRY;
		static const string INTERMEDIATE;
		static const string CINEMATIC;
		static const string ACCESSORY;
	};

	//Interface for local inventory
	class IlocalInventory { 
	public:
		virtual void addToInventory(Inventory::Item item) = 0;
		virtual void removeFromInventory(Inventory::Item item) = 0;
	};

	//Inventory for New Zealand
	class InventoryNZ : IlocalInventory {
	public:
		void addToInventory(Inventory::Item item);
		void removeFromInventory(Inventory::Item item);
		std::vector<Inventory::Item> getInventory(void);
		void setBaseProfitMargin(float);
	private:
		static float _margin;
		static int index;
		static Inventory::Item inventoryList[15];	//TODO: out of bounds error can occur on addition of items
	};

	//local Inventory for Australia
	class InventoryAU : IlocalInventory { //implement the localInventory interface
	public:
		void addToInventory(Inventory::Item item);
		void removeFromInventory(Inventory::Item item);
		std::vector<Inventory::Item> getInventory(void){ return inventoryList; };
		void setBaseProfitMargin(float);
	private:
		static float _margin;
		static std::vector<Inventory::Item> inventoryList; //private inventory list for Australia		
	};

	//append more local inventories in the future

	//Inventory context
	class InventoryContext {
	public:
		IlocalInventory* getContext() { return localInventoryContext; };
		void setContext(IlocalInventory *localInventory) { localInventoryContext = localInventory;};
	private:
		static IlocalInventory *localInventoryContext;
	};

	//Exchange rate provider
	class ExchangeRateProvider {
	public:
		Inventory::Item exchangeNZ(Inventory::Item item);
		Inventory::Item exchangeAU(Inventory::Item item);
		
		void setExchangeRates(float NZxrate, float AUxrate) {
			USD2NZD = NZxrate;
			USD2AUD = AUxrate;
		};

	private:
		static float USD2AUD;
		static float USD2NZD;
	};

	public enum Location{
		AU,
		NZ
	};

	//Basket class, to store user orders
	class Basket{
	public:
		void addItem(Inventory::Item item);
		void removeItem(Inventory::Item item);
		std::vector<Inventory::Item> getBasketList(void) { return BasketList; };
		void clearBasket(void);
		Inventory::Location getLocation(void) { return _location; };
		Basket(Location location) {_location = location;};
		~Basket() {};
	private:
		static Inventory::Location _location;
		static std::vector<Inventory::Item> BasketList;
		static int index;
	};

	//Price of basket calculator
	class BasketPrice {
	public: 
		float getPrice(Inventory::Basket basket);
	};

	//local discount applier 
	class IlocalDiscount {
	public: 
		virtual float applyDiscount(std::vector<Inventory::Item> basketList, float price) { return 0; };
		//append more functionallity in the future
	};

	//local Australian discount
	class DiscountAU: public IlocalDiscount {
	public: 
		float applyDiscount(std::vector<Inventory::Item> basketList, float price);	
	};

	class DiscountNZ :public IlocalDiscount {
	public:
		float applyDiscount(std::vector<Inventory::Item> basketList, float price);
	};
}
#endif 