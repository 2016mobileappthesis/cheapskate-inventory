#include "MyForm.h"
#include <iostream>
#include <string>
#include <vector> 
#include <algorithm>
#include <iterator>

using namespace System;
using namespace System::Windows::Forms;
using namespace std;

[STAThread]
int main(void) 
{	
	Application::EnableVisualStyles();

	CheapSkate::Initialise init;
	init.initInventory();

	Application::SetCompatibleTextRenderingDefault(false);
	CheapSkate::MyForm form;
	Application::Run(%form);
	
	return 0;
}

void CheapSkate::Initialise::initInventory(void){
	
	//initialize all elements here
	Inventory::IlocalInventory *currentInventory;
	//generate required objects
	Inventory::InventoryAU inventoryAU;
	Inventory::InventoryNZ inventoryNZ;
	Inventory::Item item;

	//init all items and push into inventory
	//The Australian inventory
	int i = 0;
	item.setAll("32 Samsung Plasma TV", Inventory::Category::ENTRY, 400, i++);
	inventoryAU.addToInventory(item);
	item.setAll("400W LG Amp/AV Receiver", Inventory::Category::ENTRY, 150, i++);
	inventoryAU.addToInventory(item);
	item.setAll("2-Channel Panasonic Speakers", Inventory::Category::ENTRY, 50, i++);
	inventoryAU.addToInventory(item);
	item.setAll("Sony Blu-ray Player", Inventory::Category::ENTRY, 80, i++);
	inventoryAU.addToInventory(item);
	item.setAll("42 Sony LCD TV", Inventory::Category::INTERMEDIATE, 900, i++);
	inventoryAU.addToInventory(item);
	item.setAll("500W Yamaha Amp/AV Receiver", Inventory::Category::INTERMEDIATE, 300, i++);
	inventoryAU.addToInventory(item);
	item.setAll("5-CH Sony Speakers", Inventory::Category::INTERMEDIATE, 120, i++);
	inventoryAU.addToInventory(item);
	item.setAll("Sony Blu-ray Player", Inventory::Category::INTERMEDIATE, 80, i++);
	inventoryAU.addToInventory(item);
	item.setAll("60 LED TV", Inventory::Category::CINEMATIC, 1800, i++);
	inventoryAU.addToInventory(item);
	item.setAll("650W Onkyo Amp/AV Receiver", Inventory::Category::CINEMATIC, 950, i++);
	inventoryAU.addToInventory(item);
	item.setAll("7-CH Bose Speaker System", Inventory::Category::CINEMATIC, 1500, i++);
	inventoryAU.addToInventory(item);
	item.setAll("Sony Blu-ray Player", Inventory::Category::CINEMATIC, 80, i++);
	inventoryAU.addToInventory(item);
	item.setAll("HDMI Cable", Inventory::Category::ACCESSORY, 5, i++);
	inventoryAU.addToInventory(item);
	item.setAll("Universal Remote", Inventory::Category::ACCESSORY, 200, i++);
	inventoryAU.addToInventory(item);
	item.setAll("Wireless Headphones", Inventory::Category::ACCESSORY, 120, i++);
	inventoryAU.addToInventory(item);
	
	//The New Zealand inventory
	i = 0;
	item.setAll("32 Samsung Plasma TV", "Entry", 400, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("400W LG Amp/AV Receiver", "Entry", 150, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("2-CH Samsung Speakers", "Entry", 50, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("Sony Blu-ray Ply", "Entry", 80, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("42 Sony LCD TV", "Intermediate", 900, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("500W Kenwood Amp/AV Receiver", "Intermediate", 300, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("5-CH Samsung Speakers", "Intermediate", 110, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("Sony Blu-ray Player", "Intermediate", 80, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("55 LED TV", "Cinematic", 1600, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("650W Onkyo Amp/AV Receiver", "Cinematic", 950, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("7-CH Bose Speaker", "Cinematic", 1500, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("Sony Blu-ray Player", "Cinematic", 80, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("HDMI Cable", "Accessory", 5, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("Universal Remote", "Accessory", 200, i++);
	inventoryNZ.addToInventory(item);
	item.setAll("Wireless Headphones", "Accessory", 120, i++);
	inventoryNZ.addToInventory(item);

}

//display all the regional inventories on the listviews
void CheapSkate::MyForm::initInventoryListview(void) {

	System::Windows::Forms::ListViewItem^ listViewItem;
	//get an instance of the AU inventory Listview
	System::Windows::Forms::ListView^ AUList;
	AUList = MyForm::getAUListViewInstance();
	//get an instance of the NZ inventory Listview
	System::Windows::Forms::ListView^ NZList;
	NZList = MyForm::getNZListViewInstance();

	std::vector<Inventory::Item>::iterator it;

	Inventory::InventoryAU inventoryAU;
	std::vector<Inventory::Item> inventoryAUList;
	inventoryAUList = inventoryAU.getInventory();

	Inventory::InventoryNZ inventoryNZ;
	std::vector<Inventory::Item> inventoryNZList;
	inventoryNZList = inventoryNZ.getInventory();

	String^ _category;
	String^ category_;
	String^ category;
	String^ product;
	String^ price;

	//iterate through items in Australian 
	for (it = inventoryAUList.begin(); it != inventoryAUList.end(); it++) {

		category_ = gcnew String(it->getCategory().c_str());
		if (category_ == _category && category != "Accessory") 
			category = " ";
		else 
			category = category_;

		_category = category_;

		product = gcnew String(it->getName().c_str());

		CheapSkate::Utility utility;
		price = utility.price2String(it->getPrice());

		listViewItem = gcnew System::Windows::Forms::ListViewItem(category);
		listViewItem->SubItems->Add(product);
		listViewItem->SubItems->Add(price);
		AUList->Items->Add(listViewItem);
	}

	//initialise the NZ listview
	for (it = inventoryNZList.begin(); it != inventoryNZList.end(); it++) {

		category_ = gcnew String(it->getCategory().c_str());
		if (category_ == _category && category != "Accessory") {
			category = " ";
		}
		else {
			category = category_;
		}
		_category = category_;

		product = gcnew String(it->getName().c_str());

		CheapSkate::Utility utility;
		price = utility.price2String(it->getPrice());

		listViewItem = gcnew System::Windows::Forms::ListViewItem(category);
		listViewItem->SubItems->Add(product);
		listViewItem->SubItems->Add(price);
		NZList->Items->Add(listViewItem);
	}
}

//push item to the display listview
void CheapSkate::MyForm::pushItemToDisp(System::Windows::Forms::ListView^ listView, Inventory::Item item) {
	
	String^ category;
	String^ product;
	String^ price;

	System::Windows::Forms::ListViewItem^ listViewItem;

	category = gcnew String(item.getCategory().c_str());
	product = gcnew String(item.getName().c_str());

	CheapSkate::Utility utility;
	price = utility.price2String(item.getPrice());
	
	listViewItem = gcnew System::Windows::Forms::ListViewItem(category);	//push to the listView display
	listViewItem->SubItems->Add(product);
	listViewItem->SubItems->Add(price);

	listView->Items->Add(listViewItem);

	return;
};

//get the home theatre system by looking for all items in category and returning all items as a vector
std::vector<Inventory::Item> CheapSkate::MyForm::getTheatreSystem(Inventory::Item item, std::vector<Inventory::Item> localInventoryList) {

	//get the strings definitions of the categories
	string Entry = Inventory::Category::ENTRY;
	string Intermediate = Inventory::Category::INTERMEDIATE;
	string Cinematic = Inventory::Category::CINEMATIC;

	std::vector<Inventory::Item> categoryList;

	std::vector<string> itemRecord;
	bool itemExists = false;

	string ItemCategory = item.getCategory();

	//group the items together if one item is chosen from the Entry, Intermediate or Cinematic categories
	if (ItemCategory == Entry) {
		//get all entry items
		std::vector<Inventory::Item>::iterator _it;
		for (_it = localInventoryList.begin(); _it != localInventoryList.end(); _it++) {
			if (_it->getCategory() == ItemCategory) {
				categoryList.push_back(*_it);
			}			
		}
	}
	else if (ItemCategory == Intermediate) {
		//get all intermediate items
		std::vector<Inventory::Item>::iterator _it;
		for (_it = localInventoryList.begin(); _it != localInventoryList.end(); _it++) {
			if (_it->getCategory() == Intermediate) {
				categoryList.push_back(*_it);
			}
		}
	}
	else if (ItemCategory == Cinematic) {
		//get all cinematic items
		std::vector<Inventory::Item>::iterator _it;
		for (_it = localInventoryList.begin(); _it != localInventoryList.end(); _it++) {
			if (_it->getCategory() == Cinematic) {
				categoryList.push_back(*_it);
			}
		}
	}

	//able to append more categories
	else {
		categoryList.push_back(item);
	}

	return categoryList;
}

//
std::vector<Inventory::Item> CheapSkate::MyForm::removeTheatreSystem(Inventory::Item item, std::vector<Inventory::Item> localInventoryList) {

	std::vector<Inventory::Item> categoryList;
	std::vector<string> itemRecord;

	bool itemExists = false;
	bool firstItem = true;
	string ItemCategory = item.getCategory();

	//group the items together if one item is chosen from the Entry, Intermediate or Cinematic categories
	if (ItemCategory != Inventory::Category::ACCESSORY) {
		//get all entry items
		std::vector<Inventory::Item>::iterator _it;
		for (_it = localInventoryList.begin(); _it != localInventoryList.end(); _it++) {
			
			if (_it->getCategory() == ItemCategory) {
				itemExists = false;
				if (firstItem) {
					categoryList.push_back(*_it);
					firstItem = false;
				}
				else {
					auto pv = std::prev(_it, 1);
					itemRecord.push_back(pv->getName()); //get the previous item name
					
					std::vector<string>::iterator i;
					for (i = itemRecord.begin(); i != itemRecord.end(); i++) { //look for the item
						if (*i == _it->getName()) {
							itemExists = true;
							break;
						}
					}
					if (!itemExists) {
						categoryList.push_back(*_it);
					}
				}
			}
		}
	}

	else {
		categoryList.push_back(item);
	}

	return categoryList;
}

//Handle add to basket clicked event
void CheapSkate::MyForm::AddBasketClicked(void) {
	
	std::vector<Inventory::Item> localInventoryList;
	int count = 0, index = 0;
	Inventory::Basket basket(location);

	std::vector<Inventory::Item>::iterator it;

	//for each possible region
	switch (location) 
	{
		case Inventory::Location::AU: {
			count = listView2->SelectedItems->Count;			//Get the number of items selected 
			if (count > 0) {									//assign variables if item is selected; count>0	
				index = listView2->FocusedItem->Index;			//get index of the item
				Inventory::InventoryAU inventoryAU;				//get the local inventory
				localInventoryList = inventoryAU.getInventory();
			}
			else
				return;
			};
			break;
		case Inventory::Location::NZ: {
			count = listView4->SelectedItems->Count;
			if (count > 0) {
				index = listView4->FocusedItem->Index;
				Inventory::InventoryNZ inventoryNZ;
				localInventoryList = inventoryNZ.getInventory();
			}
			else
				return;
			};
			break;
		default:
			break;
	}
	
	for (it = localInventoryList.begin(); it != localInventoryList.end(); it++) {		//iterate through inventory list to find the item selected

		//if index matches in the local inventory, push item to basket and the listview				
		if (it->getIndex() == index)
		{
			//get the listView instance of the listview item is to be displayed on
			System::Windows::Forms::ListView^ BasketList;
			BasketList = MyForm::getBasketListViewInstance();

			std::vector<Inventory::Item> categoryList = getTheatreSystem(*it, localInventoryList);

			//Display the items of interest to the listview
			std::vector<Inventory::Item>::iterator _it;
			for (_it = categoryList.begin(); _it != categoryList.end(); _it++) {
				pushItemToDisp(BasketList, *_it);
				basket.addItem(*_it);
			}

			updateBasketPrice(basket, label3);

			//Done all that is required, no need to continue in the For loop
			return;
		}
		
	}
}

//Handle remove from basket clicked event
void CheapSkate::MyForm::RemoveBasketClicked(void) {

	Inventory::Basket basket(location);

	std::vector<Inventory::Item> basketList;
	basketList = basket.getBasketList();

	std::vector<Inventory::Item> _basketList(basketList);
	std::vector<Inventory::Item> categoryList;
	std::vector<Inventory::Item>::iterator it, _it;

	//get count of items selected on the listView
	int count = listView3->SelectedItems->Count;
	if (count > 0) {

		//get the index of the item selected
		int in = listView3->FocusedItem->Index;
		
		bool itemExists = false;
		bool firstItem = true;
		int index = 0;
		//get the selected item
		Inventory::Item selectedItem = getSelectedItem(basketList,in);
		//get the corresponding items to be removed as a vector
		categoryList = removeTheatreSystem(selectedItem, basketList);
		std::vector<string> itemRecord;

		//iterate through the list to be deleted and remove corresponding items from the basket	
		for (it = categoryList.begin(); it != categoryList.end(); it++) {	
			itemExists = false;
			for (_it = _basketList.begin(); _it != _basketList.end() ; _it++) {
				//if the corresponding item is found then remove from the basket
				if ((it->getName() == _it->getName()) && (it->getCategory() == _it->getCategory())) {										
						if (!itemExists) { 
							basket.removeItem(*_it); 
							itemExists = true;
						}					
					}
				}
			}
		}		
		//refresh the basket listView and write again
		listView3->Items->Clear();		//TODO: implement as function
		basketList = basket.getBasketList();

		for (it = basketList.begin(); it != basketList.end(); it++) {
			System::Windows::Forms::ListView^ basketListView;
			basketListView = MyForm::getBasketListViewInstance();
			pushItemToDisp(basketListView, *it);	
		}
		updateBasketPrice(basket, label3);
}

//Handle location tab clicked event
void CheapSkate::MyForm::LocationTabClicked(void) {
	
	int index = tabControl1->SelectedIndex;		//get tab index

	switch (index) {
		case Inventory::Location::AU: {			//Australian tab selected
			location = Inventory::Location::AU;
			Inventory::Basket basket(location);
			basket.clearBasket();
			listView3->Items->Clear();			//replace with defined values
			label6->Text = "(AUD)";
			label7->Text = "(AU)";
			}
			break;
		case Inventory::Location::NZ: {			//NZ tab selected
			location = Inventory::Location::NZ;
			Inventory::Basket basket(location);
			basket.clearBasket();
			listView3->Items->Clear();			//replace with defined values
			label6->Text = "(NZD)";
			label7->Text = "(NZ)";
			}
			break;							  
		default:
			break;
	}
}

void CheapSkate::MyForm::ConfirmOrderClicked(void) {

	//create a order
	Order::order _order;
	Inventory::Basket basket(location);
	Inventory::BasketPrice basketPrice;

	_order.setOrderList(basket);
	_order.setOrderPrice(basketPrice.getPrice(basket));
	_order.setOrderStatus(Order::orderStatus::CREATED);

	CheapSkate::MyOrder^ myorder = gcnew CheapSkate::MyOrder();
	myorder->ShowDialog();
}

void CheapSkate::MyForm::updateBasketPrice(Inventory::Basket basket, System::Windows::Forms::Label^ basketPriceLabel) {

	Inventory::IlocalDiscount* discountApplier;

	switch (location){							
		case Inventory::Location::AU: {
			Inventory::DiscountAU _discountApplier;			
			discountApplier = &_discountApplier;
			};
			break;
		case Inventory::Location::NZ: {
			Inventory::DiscountNZ _discountApplier;
			discountApplier = &_discountApplier;
			};
			break;
		default:
			break;
	}

	//Update the basket price and display on the price label  
	Inventory::BasketPrice basketPrice;
	float total_price = basketPrice.getPrice(basket);
	float final_price = discountApplier->applyDiscount(basket.getBasketList(), total_price);

	CheapSkate::Utility ut;
	String^ _price = ut.price2String(final_price);

	label3->Text = _price;	//TODO: use declared variable instead of magic value
}

Inventory::Item CheapSkate::MyForm::getSelectedItem(std::vector<Inventory::Item> localInventoryList, int index) {
	
	int in = 0;
	//iterate through basketList and get selected item
	std::vector<Inventory::Item>::iterator it;
	for (it = localInventoryList.begin(); it != localInventoryList.end(); it++) {
		//if the index matches the 
		if (index == in) {
			return *it;
		}
		in++;
	}	 
	return *it;
}

