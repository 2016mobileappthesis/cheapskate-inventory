#include "MyOrder.h"
#include "Order.h"

void CheapSkate::MyOrder::initMyOrder(void){
	Order::order _order;

	//float a = _order.getOrderPrice();
	CheapSkate::Utility ut;
	String^ _price = ut.price2String(_order.getOrderPrice());
	label7->Text = _price;
}

void CheapSkate::MyOrder::OnConfirmClicked(void){
	
	Order::order _order;
	_order.setOrderStatus(Order::orderStatus::ORDERED);

	//generate dialogbox to notify user
	std:vector<Inventory::Item> orderList = _order.getOrderList();
	
	//generate the string to go into the dialog box
	String^ startMsg = "Thank you " + textBox1->Text + ", your order has been confirmed\n";
	string _orderList = "You have ordered the following:\n";
	std::vector<Inventory::Item>::iterator it;

	for (it = orderList.begin(); it != orderList.end(); it++) {
		_orderList += "\n* ";
		_orderList += it->getName();
	}

	_orderList += "\n\nWould you like the package to be delivered now?";
	String^ msg = gcnew String(_orderList.c_str());
	msg = startMsg + msg;

	String^ cap = "Order Notification";
	System::Windows::Forms::MessageBoxButtons buttons = MessageBoxButtons::YesNo;
	System::Windows::Forms::DialogResult result;

	result = MessageBox::Show(msg,cap,buttons, MessageBoxIcon::Information);

	//send the order
	switch (result) {
		case System::Windows::Forms::DialogResult::Yes: {
			_order.setOrderStatus(Order::orderStatus::DELIVERED);
			System::Windows::Forms::MessageBoxButtons buttons = MessageBoxButtons::OK;
			msg = "Your order has been delivered";
			MessageBox::Show(msg,cap, buttons, MessageBoxIcon::Information);		
			//clear the Order::order object 
			label7->Text = "$0.00"; //should clear the order object and then reprint price of order
			}
			break;
		case System::Windows::Forms::DialogResult::No: {
			_order.setOrderStatus(Order::orderStatus::READYFORDELIVERY);
			System::Windows::Forms::MessageBoxButtons buttons = MessageBoxButtons::OK;
			msg = "Your order is ready for delivery";
			MessageBox::Show(msg,cap, buttons, MessageBoxIcon::Information);
			label7->Text = "$0.00";
			}
			break;
		default:
			break;
	}

}

String^ CheapSkate::Utility::price2String(float price) {

	int dpval = 0, dp = 0, length = 0;

	std::string _Price;
	_Price = std::to_string(price);

	for (const auto& c : _Price) {
		if (c == '.') {
			dp = 1;
		}
		dpval += dp;
		length++;
	}

	switch (dpval) {
	case 0: {//0 dp
		_Price.append(".00");
	}
	case 1: {//1 dp
		_Price.append("0");
	}
			break;
	case 2: {//2 dp			
	}
			break;

	default: //too many dp, keep only 2
		_Price = _Price.substr(0, length - dpval + 3);
		break;
	}
	_Price = "$" + _Price;

	String^ Price = gcnew String(_Price.c_str());;
	return Price;

}