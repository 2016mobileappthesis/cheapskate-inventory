#include "Inventory.h"

using namespace System;
using namespace System::Windows::Forms;
using namespace std;

//Category class
const string Inventory::Category::ENTRY = "Entry";
const string Inventory::Category::INTERMEDIATE = "Intermediate";
const string Inventory::Category::CINEMATIC = "Cinematic";
const string Inventory::Category::ACCESSORY = "Accessory";

//InventoryNZ class
Inventory::Item Inventory::InventoryNZ::inventoryList[15]; //TODO: Access using pointer, out of bounds error 
int Inventory::InventoryNZ::index = 0;
float Inventory::InventoryNZ::_margin = 1.2;

void Inventory::InventoryNZ::addToInventory(Inventory::Item item) {
	ExchangeRateProvider ex;

	item = ex.exchangeNZ(item);
	float _price = item.getPrice()*_margin;
	item.setPrice(_price);

	inventoryList[index] = item;
	index++;
}

void Inventory::InventoryNZ::removeFromInventory(Inventory::Item item) {
	//
}

std::vector<Inventory::Item> Inventory::InventoryNZ::getInventory()
{
	std::vector<Inventory::Item> inventoryVector(inventoryList, inventoryList + sizeof inventoryList/sizeof inventoryList[0]);
	return inventoryVector;
}

void Inventory::InventoryNZ::setBaseProfitMargin(float margin) {
	_margin = margin;
};

//InventoryAU class
std::vector<Inventory::Item> Inventory::InventoryAU::inventoryList;
float Inventory::InventoryAU::_margin = 1.2;

void Inventory::InventoryAU::addToInventory(Inventory::Item item) {
	ExchangeRateProvider ex;

	item = ex.exchangeAU(item);
	float _price = item.getPrice()*_margin;
	item.setPrice(_price);

	inventoryList.push_back(item);
}

void Inventory::InventoryAU::removeFromInventory(Inventory::Item item) {

	std::vector<Inventory::Item>::iterator it;
	for (it = inventoryList.begin(); it != inventoryList.end(); it++) {
		if (it->getName() == item.getName()) {		
		}
	}
}

void Inventory::InventoryAU::setBaseProfitMargin(float margin) {
	_margin = margin;
};

//ExchangeRateProvider class
float Inventory::ExchangeRateProvider::USD2AUD = 1.35;
float Inventory::ExchangeRateProvider::USD2NZD = 1.45;

Inventory::Item Inventory::ExchangeRateProvider::exchangeNZ(Inventory::Item item) {
	float price =  item.getPrice();
	
	if (USD2NZD != 0) {
		price = price*USD2NZD;
		item.setPrice(price);
	}
	return item;
}

Inventory::Item Inventory::ExchangeRateProvider::exchangeAU(Inventory::Item item) {
	float price = item.getPrice();

	if (USD2AUD != 0) {
		price = price*USD2AUD;
		item.setPrice(price);
	}
	return item;
}

//Basket class
Inventory::Location Inventory::Basket::_location;
std::vector<Inventory::Item> Inventory::Basket::BasketList;
int Inventory::Basket::index = 0;

void Inventory::Basket::addItem(Inventory::Item item) {	
	
	item.setIndex(index);
	index++;
	BasketList.push_back(item);
}

void Inventory::Basket::removeItem(Inventory::Item item) {

	std::vector<Inventory::Item>::iterator it, it_;

	for (it = BasketList.begin(); it != BasketList.end(); it++) {
		if (it->getName() == item.getName() && it->getCategory() == item.getCategory()) {
			it_ = it;
		}	
	}
	BasketList.erase(it_);
	index--;
}

void Inventory::Basket::clearBasket(void) {
	BasketList.clear();
}

//BasketPrice class
float Inventory::BasketPrice::getPrice(Inventory::Basket basket) {
	
	float price = 0;
	
	Inventory::Location l;
	l = basket.getLocation();

	std::vector<Inventory::Item> BasketList = basket.getBasketList();
	std::vector<Inventory::Item>::iterator it;
	
	for (it = BasketList.begin(); it != BasketList.end(); it++) {	
		price += it->getPrice();									//get the price of each item and accumulate	
	}

	return price;
}

//Discount class
float Inventory::DiscountAU::applyDiscount(std::vector<Inventory::Item> basketList, float price) {
	
	std::vector<Inventory::Item>::iterator it;
	float _price = 0, discount = 0;
	int count = 0;
	bool eligible = false;

	for (it = basketList.begin(); it != basketList.end(); it++) {
		string category = it->getCategory();
		if (category == Inventory::Category::ENTRY || category == Inventory::Category::INTERMEDIATE || category == Inventory::Category::CINEMATIC) {
			eligible = true;
			break;
		}
	}
	
	if (eligible) {	//eligible for discount
		for (it = basketList.begin(); it != basketList.end(); it++) {	//cycle through and locate all accessory items
			if (it->getCategory() == Inventory::Category::ACCESSORY) {
				count++;
				_price += it->getPrice();
			}
		}
		
		if (count > 3)	//discount 10% per accessory, up to 30%
			count = 3;
		discount = 0.1*count;

		_price = _price * discount;
		price = price - _price;
		return price;	
	}
	else
		return price;
}

float Inventory::DiscountNZ::applyDiscount(std::vector<Inventory::Item> basketList, float price) {
	
	std::vector<Inventory::Item>::iterator it;
	float _price = 0, discount = 0;
	int count = 0;
	bool eligible = false;
	
	for (it = basketList.begin(); it != basketList.end(); it++) {
		string category = it->getCategory();
		if (category == Inventory::Category::ENTRY || category == Inventory::Category::INTERMEDIATE || category == Inventory::Category::CINEMATIC) {
			eligible = true;
			break;
		}
	}

	if (eligible) {	//eligible for discount
		for (it = basketList.begin(); it != basketList.end(); it++) {	//cycle through and locate all accessory items
			if (it->getCategory() == Inventory::Category::ACCESSORY) {
				count++;
				_price += it->getPrice();
			}
		}

		if (count > 3)	//discount 10% per accessory, up to 30%
			count = 3;
		discount = 0.1*count;

		_price = _price * discount;
		price = price - _price;
		return price;
	}
	else
		return price;
}
