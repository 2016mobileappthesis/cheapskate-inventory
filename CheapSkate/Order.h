#ifndef __ORDER_H_INCLUDED__
#define __ORDER_H_INCLUDED__
#pragma once

#include "Inventory.h"
#include <vector>

using namespace System;
using namespace std;

namespace Order {

	public enum orderStatus {
		CREATED,
		ORDERED,
		READYFORDELIVERY,
		DELIVERED
	};

	class order {
	public: 
		void setOrderStatus(Order::orderStatus);
		Order::orderStatus getOrderStatus(void);
		void setOrderPrice(float);
		float getOrderPrice(void);
		void setOrderList(Inventory::Basket basket) { orderList = basket.getBasketList(); };
		std::vector<Inventory::Item> getOrderList(void) { return orderList; };

	private:
		static std::vector<Inventory::Item> orderList;
		static Order::orderStatus _orderStatus;
		static float _orderPrice;	
	};


}

#endif